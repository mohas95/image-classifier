#################################
# load trained model checkpoint #
#################################
import torch
import torchvision
from torchvision import models

def load_checkpoint(filepath,device):
    
    resnet18 = models.resnet18(pretrained=True)
    alexnet = models.alexnet(pretrained=True)
    vgg16 = models.vgg16(pretrained=True)
    
    
    checkpoint = torch.load(filepath, map_location = 'cpu')
    
    model_dict = {'resnet18': resnet18, 'alexnet': alexnet, 'vgg16': vgg16}  
    
    model = model_dict[checkpoint['architecture']]    
    
    for param in model.parameters():
        param.requires_grad = False
        
    model.classifier = checkpoint['classifier']
    model.load_state_dict(checkpoint['state_dict'])

    return model.to(device)

def load_optimizer(filepath):
    checkpoint = torch.load(filepath)
    optimizer = optim.SGD(model.classifier.parameters(), lr=0.001)
    optimizer.load_state_dict(checkpoint['optimizer'])
    criterion = checkpoint['error_function']
    epoch = checkpoint['epoch_number']
    
    return optimizer, epoch
