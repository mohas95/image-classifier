###############################
# get commandline arguements  #
###############################

import argparse

def get_args():
    parser = argparse.ArgumentParser()
    
    parser.add_argument('data_dir', type = str, default = 'flowers/', help = 'path to the folder of training images')
    parser.add_argument('--arch', type = str, default = 'vgg16', help = 'The CNN model architecture used to classify dataset')
    parser.add_argument('--save', type = str, default = 'checkpoint.pth', help = 'save path')
    parser.add_argument('--learning_rate', type = float, default = 0.001, help = 'learning rate')
    parser.add_argument('--hidden_units', type = int, default = 512, help = 'hidden layer units')
    parser.add_argument('--epochs', type = int, default = 20, help = 'number of training iterations ')
    parser.add_argument('--gpu', type = bool, default = False, help = 'toggle gpu')

    return parser.parse_args()