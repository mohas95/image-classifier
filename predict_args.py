###############################
# get commandline arguements  #
###############################

import argparse

def get_args():
    parser = argparse.ArgumentParser()
    
    parser.add_argument('img_file', type = str, help = 'path to the folder of training images')
    parser.add_argument('load_checkpoint', type = str, default = 'checkpoint.pth', help = 'load the checkpoint')
    parser.add_argument('--top_k', type = int, default = 3, help = 'top number of predictions')
    parser.add_argument('--category_names', type = str, default = 'cat_to_name.json', help = 'load the category key')
    parser.add_argument('--gpu', type = bool, default = False, help = 'use gpu to do computations')

    
    return parser.parse_args()