#####################################
# run the prediction model on image #
#####################################
import torch
from torchvision import models

def predict(img, model, topk, device):
    ''' Predict the class (or classes) of an image using a trained deep learning model.
    '''
    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
    with torch.no_grad():
        model.eval()
        # TODO: Implement the code to predict the class from an image file
        input=img.unsqueeze(0)
        input= input.to(device)
        output = model.forward(input.to(device))
        ps = torch.exp(output)
        top_p, top_class= torch.topk(ps,topk,dim=1)

    return  top_p.tolist(), top_class.tolist()